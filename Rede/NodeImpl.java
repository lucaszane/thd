package Rede;

import java.util.List;
import java.util.ArrayList;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class NodeImpl extends UnicastRemoteObject implements Node {
    private int tcpPort;
    //private InetAddress nodeaddress;
    private Node successor;
    private Node predecessor;
    private List<Entidade> entidades;
    private String nodeKey;
    private String rmiaddress;
    private String command;
    
    public NodeImpl () throws RemoteException {
        super();
        this.setId(Hex.getRandomHexString(64));
        this.entidades = new ArrayList<>();
        this.command = "";
    }
    
    public Node getNext() {
        return this.successor;
    }
    public Node getPrev() {
        return this.predecessor;
    }
    public void setNext(Node n) {
        this.successor = n;
    }
    public void setPrev(Node n) {
        this.predecessor = n;
    }
    public List<Entidade> getEntidades() {
        return this.entidades;
    }
    
    public void setEntidades(List<Entidade> entidades) {
        this.entidades = entidades;
    }
    public void addEntidade(Entidade e) {
        this.entidades.add(e);
    }
    public String getId() {
        return this.nodeKey;
    }
    public void setId(String id) {
        this.nodeKey = id;
    }
    
    public void send(String s) {
		this.command = s;
	}	
	
    public String next() {
        while(this.command.equals("")) {
            System.out.println(this.command);
        }
        String aux = this.command;
		this.command = "";
		return aux;
	}	
}