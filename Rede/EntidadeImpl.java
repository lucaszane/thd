package Rede;
import java.nio.charset.StandardCharsets;

public class EntidadeImpl implements Entidade{
    private String key;
    private byte[] value;
    
    public void setKey(String key){
        this.key = Hash.getHash(key);
    }
    
    public String getKey () {
        return this.key;
    }
    
    public void setValue (byte[] newValue) {
        this.value = newValue;
    }
    
     public byte[] getValue () {
        return this.value;
    }
    
}