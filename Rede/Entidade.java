package Rede;

import java.util.List;

public interface Entidade extends java.io.Serializable {
    public String getKey();
    public void setKey(String key);
    public byte[] getValue();
    public void setValue(byte[] value);
}