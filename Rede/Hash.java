package Rede;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hash {
    public static String getHash(String key){
        try {
			MessageDigest m = MessageDigest.getInstance("SHA-256");
			m.update(key.getBytes(), 0 , key.length());
		    byte[] digest = m.digest();
		    key = new BigInteger(1,digest).toString(16);
		} 
		catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
        return key;
    }
}