package Rede;

import java.util.Random;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.math.BigInteger;

public class Hex {
    public static String getRandomHexString(int numchars){
        Random r = new Random();
        StringBuffer sb = new StringBuffer();
        while (sb.length() < numchars){
            sb.append(Integer.toHexString(r.nextInt()));
        }

        return sb.toString().substring(0, numchars);
    }
    public static String getHash(String key) {
        try {
			MessageDigest m = MessageDigest.getInstance("SHA-256");
			m.update(key .getBytes(), 0 , key.length());
		    byte[] digest = m.digest();
		    key = new BigInteger(1, digest).toString(16);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return key;
    }
}