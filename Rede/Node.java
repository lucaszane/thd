package Rede;


import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;


public interface Node extends Remote {
    public Node getNext() throws RemoteException;
    public Node getPrev() throws RemoteException;
    public String getId() throws RemoteException;
    public void setId(String id) throws RemoteException;
    public void setNext(Node n) throws RemoteException;
    public void setPrev(Node n) throws RemoteException;
    public void addEntidade(Entidade e) throws RemoteException;
    public List<Entidade> getEntidades() throws RemoteException;
    public void send(String s) throws RemoteException;
    public String next() throws RemoteException;
    public void setEntidades(List<Entidade> entidades) throws RemoteException;
}