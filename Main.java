import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.rmi.registry.*;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;
import java.nio.charset.StandardCharsets;
import java.io.ByteArrayOutputStream;
import Rede.*;

class Main {
    
    public static NodeImpl join(Registry r) throws RemoteException, NotBoundException {
        String[] nodes = r.list();
        NodeImpl node = null;
        int i;
        
        for (i=0; i<nodes.length; i++) {
            try {
                node = insereNode((Node) r.lookup(nodes[i]));
                System.out.println("Node: "+ node.getId());
                System.out.println("Anterior:" + node.getPrev().getId());
                System.out.println("Próximo:" + node.getNext().getId());
                break;
            }
            catch (RemoteException e) {
            }
        }
        if (i >= nodes.length){
            node = new NodeImpl();
            node.setNext(node);
            node.setPrev(node);
        }
        return node;
    }
    
    public static NodeImpl insereNode(Node node) throws RemoteException, NotBoundException {
        // cria nova node
        NodeImpl newNode = new NodeImpl();
        
        // encontra posição para inserir nova Node
        node = getLowestBiggerNode(node, newNode.getId());
        Node nextNode = node.getNext();
        
        // atualiza ponteiros
        node.setNext(newNode);
        newNode.setPrev(node);
        nextNode.setPrev(newNode);
        newNode.setNext(nextNode);
        
        // rebalanceando entidades com o novo Node
        // id >= k
        List<Entidade> entidades = new ArrayList<>();
        List<Entidade> entidadesNext = new ArrayList<>();
        List<Entidade> e = nextNode.getEntidades();
        
        for (int i=0; i < e.size(); i++) {
            if (newNode.getId().compareTo(e.get(i).getKey()) >= 0) {
                entidades.add(e.get(i));
            }
            else {
                entidadesNext.add(e.get(i));
            }
        }
        nextNode.setEntidades(entidadesNext);
        newNode.setEntidades(entidades);
        return newNode;
    }
    
    public static Node getLowestBiggerNode(Node node, String key) throws RemoteException {
        if (node.getId().compareTo(key) >= 0) {
            while (node.getPrev().getId().compareTo(key) >= 0 && node.getId().compareTo(node.getPrev().getId()) > 0) {
                node = node.getPrev();
            }
            node = node.getPrev();
        }
        else {
            while (key.compareTo(node.getNext().getId()) > 0 && node.getId().compareTo(node.getNext().getId()) < 0 ) {
                node = node.getNext();
            }
        }
        return node;
    }
    
    public static void leave(Node node) throws RemoteException {
        Node nextNode = node.getNext();
        nextNode.setPrev(node.getPrev());
        node.getPrev().setNext(nextNode);
        List<Entidade> e = nextNode.getEntidades();;
        
        for (int i=0; i < node.getEntidades().size(); i++) {
            e.add(node.getEntidades().get(i));
            nextNode.setEntidades(e);
        }
    }
    
    
    public static void store(String key, byte[] value, Node node) throws RemoteException, NotBoundException {
        Entidade entity = new EntidadeImpl();
        entity.setKey(key);
        entity.setValue(value);
        key = Hash.getHash(key);

        // encontra node responsavel pela key
        node = getLowestBiggerNode(node, key);
        List<Entidade> entidades = node.getNext().getEntidades();
        int i = 0;
        
        while (i < entidades.size() && entidades.get(i).getKey().compareTo(key) != 0) {
            i++;
            
        }
        
        if (i == entidades.size()) {
            List<Entidade> e = node.getNext().getEntidades();
            e.add(entity);
            node.getNext().setEntidades(e);
        }
        else {
            List<Entidade> e = node.getNext().getEntidades();
            e.get(i).setValue(value);
            node.getNext().setEntidades(e);
        }
        System.out.println("Stored in node: "+node.getNext().getId());
    }
    
    public static List<Node> list(Node node) throws RemoteException, NotBoundException {
        List<Node> l = new ArrayList<>();
        String firstId = node.getId();
        l.add(node);
        node = node.getNext();
        
        while(node.getId().compareTo(firstId) != 0) {
            l.add(node);
            node = node.getNext();
        }
        return l;
    }
    
    public static byte[] retrieve(String key, Node node) throws RemoteException, NotBoundException {
        byte[] value = new byte[10];
        node = getLowestBiggerNode(node, key);
        node = node.getNext();
        List<Entidade> entidades = node.getEntidades();
        
        for (int i = 0; i < entidades.size(); i++) {
            if (entidades.get(i).getKey().compareTo(key) == 0) {
                return entidades.get(i).getValue();
            }
        }
        System.out.println("Não há informações cadastradas.");
        return value;
    }
    
    
    
    public static void main(String[] args) throws RemoteException, UnknownHostException, InterruptedException {
        Registry r = LocateRegistry.getRegistry(null);
        Scanner scan = new Scanner(System.in);
			
        NodeImpl node = new NodeImpl();
        try {
            node = join(r);
        }
        catch (NotBoundException e) {
            System.out.println(e);
        }
        
        try {
            r.bind(node.getId(), node);
        }
        catch (AlreadyBoundException e) {
            System.out.println(e);
        }
        
        System.out.println("This node: " + node.getId());

        while (true) {
            String comando = scan.next();
            //System.out.println(comando);
            
            if (comando.compareTo("store") == 0){
                try {
                    String key = scan.next();
                    store(key, scan.next().getBytes(), node);
                }
                catch (NotBoundException e) {
                    System.out.println(e);
                }
            }
            
            else if(comando.compareTo("retrieve") == 0 ) {
                try {
                    String key = scan.next();
                    key = Hash.getHash(key);
                    System.out.println(new String(retrieve(key,node),StandardCharsets.UTF_8));
                }
                catch (NotBoundException e) {
                    System.out.println(e);
                }
            }
            
            else if(comando.compareTo("list") == 0 ) {
                try {
                    List<Node> lista = list(node);
                    
                    for (int i=0; i < lista.size(); i++) {
                        System.out.println("entity_key: " + lista.get(i).getId() + "\tsize: " + lista.get(i).getEntidades().size());
                    }
                }
                catch (NotBoundException e) {
                    System.out.println(e);
                }
            }
            
            else if(comando.compareTo("leave") == 0 ) {
                try {
                    List<Node> lista = list(node);
                    if (lista.size() > 1) { 
                        leave(node);
                        System.out.println("YOU LEFT");
                    }
                    System.exit(0);
                }
                catch (NotBoundException e) {
                    System.out.println(e);
                }
            }
            else if (comando.compareTo("new_client") == 0){
                try {
                    String clientName = scan.next();
                    store(clientName, "".getBytes() , node);
                    clientMode(clientName, node);
                }
                catch (NotBoundException e) {
                    System.out.println(e);
                }
            }
            else if (comando.compareTo("get_client") == 0){
                try {
                    String clientName = scan.next();
                    clientMode(clientName, node);
                }
                catch (Exception e) {
                    System.out.println(e);
                }
            }
            else {
                System.out.println("ERRO! Comando não encontrado!");
            }
        }
    }
    
    public static void clientMode (String clientNamekey, Node node) {
        System.out.println("Você está no client " + clientNamekey + ", para sair digite exit");
        Scanner scan = new Scanner(System.in);
        byte[] clientInfos = "".getBytes();
        while(true) {
            String comando = scan.next();
            try{
                clientInfos = retrieve(Hash.getHash(clientNamekey), node);
            }
            catch(Exception e){
                System.out.println(e);
            }
            if (comando.compareTo("new_information") == 0){
                String key = scan.next();
                byte[] info = scan.nextLine().trim().getBytes();
                try{
                    store(clientNamekey + key, info, node);
                    clientInfos = retrieve(Hash.getHash(clientNamekey), node);
                    String str = new String(clientInfos, StandardCharsets.UTF_8);
                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    if(!str.equals("")){
                        outputStream.write(clientInfos);
                        outputStream.write("|".getBytes());
                    }
                    outputStream.write(key.getBytes());
                    store(clientNamekey, outputStream.toByteArray( ), node);
                }
                catch(Exception e) {
                    System.out.println(e);
                }
            }
            else if (comando.compareTo("get_informations") == 0){
                try{
                    String str = new String(clientInfos, StandardCharsets.UTF_8);
                    String[] keys = str.split("\\|");
                    for (int i = 0; i < keys.length; i++) {
                        String info = new String(retrieve(Hash.getHash(clientNamekey + keys[i]), node), StandardCharsets.UTF_8);
                        System.out.println(keys[i] + ": " + info);
                    }
                }
                catch(Exception e) {
                    System.out.println(e);
                }
            }
            else if (comando.compareTo("exit") == 0){
                System.out.println("Você está de volta ao servidor.");
                break;
            }
            else {
                System.out.println("ERRO! Comando não encontrado!");
            }
        }
    }
    
}